import React, {Fragment, useContext} from 'react';
import {ListContext} from './ListContext';

function List() {
    const {elements, addElement} = useContext(ListContext);

    return (
        <Fragment>
            <button
                onClick={() => {
                    addElement('x')
                }}
            >
                Add Element
            </button>
            <ul>
                {elements.map((elem) => <li>{elem}</li>)}
            </ul>
        </Fragment>
    );
}

export default List;