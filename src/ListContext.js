import React, { useState} from 'react';

export const ListContext = React.createContext();

export const ListProvider = (props) => {
    let [elements, setElements] = useState(['a', 'b', 'c']);

    function addElement(newElement) {
        setElements([
            ...elements,
            newElement
        ]);
    }

    return (
        <ListContext.Provider value={{
            elements,
            addElement
        }}>
            {props.children}
        </ListContext.Provider>
    );
}