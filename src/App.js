import React, {Component} from 'react';
import List from "./List";
import {ListProvider} from "./ListContext";

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="App">
                <ListProvider>
                    <List/>
                </ListProvider>
            </div>
        );
    }
}

export default App;
